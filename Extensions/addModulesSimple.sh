#!/bin/bash

remotePath=git@gitlab.com:volvo_gto/gpss_mvp
subdir="src/"

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
arg1=$1
scriptDir=$(
	cd $(dirname $sourceDir)
	pwd
)

if [[ $sourceDir != $runDir ]]; then
	echo "ERROR: You must run this script."
	return 1
fi

printHelp() {
	echoc BWHITE "NAME"
	echo "	addModules "
	echoc BWHITE "SYNOPSIS"
	echoc NOCOLOR "	addModules [" YELLOW "filename" NOCOLOR "]"
	echoc BWHITE "DESCRIPTION"
	echoc NOCOLOR "	addModules adds the git-repos listed in the file " YELLOW "filename " NOCOLOR "as git submodules in the target path defined by the dirs in each git-repo."
	echoc NOCOLOR "	If " YELLOW "filename " NOCOLOR "is not provided, then the default file " LGREEN "modules.txt " NOCOLOR "will be used."
	echoc NOCOLOR "	For example, if the " YELLOW "filename " NOCOLOR "has the item " LGREEN "shared/gpss_interfaces/atr_state_msgs" NOCOLOR ", the git repo "
	echoc LBLUE "        git@gitlab.com:volvo_gto/gpss_mvp/" LGREEN "shared/gpss_interfaces/atr_state_msgs" NOCOLOR ".git will be added in the path " BWHITE "workspace/src/shared/gpss_interfaces/"
	echoc BWHITE "AUTHORS"
	echoc NOCOLOR "Emmanuel Dean <dean@chalmers.se>"
}

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
	printHelp
	exit 1
fi

if [[ $# == 0 ]]; then
	filename="modules.txt"
fi

if [[ $# == 1 ]]; then
	filename=$1
fi

modules=($(cat $filename))
len=${#modules[@]}

for ((i = 0; i < len; i++)); do

	echo "----------------------------"
	module=${modules[${i}]}

	if [[ $module == *"/"* ]]; then
		echoc GREEN "Git-repo with subgroup"
		xpath=${module%/*}/
		basename_ext=${module##*/}
		basename=${basename_ext%.*}
		dest=${subdir}${xpath}
		oarg="-d"
	else
		echoc YELLOW "Git-repo without subgroup"
		basename_ext=${module##*/}
		basename=${basename_ext%.*}
		xpath=""
		dest=${subdir}${basename}
		oarg=""
	fi

	# echoc RED "xpath: $xpath"
	# echoc YELLOW "basename_ext: $basename_ext"
	# echoc LBLUE "basename: $basename"
	# echoc LGREEN "xext: $xext"
	echo $dest

	gitRepo=${remotePath}/${xpath}${basename}.git

	echo "gitsadd $oarg $gitRepo $dest"

	echo "1" | gitsadd $oarg $gitRepo $dest

done
