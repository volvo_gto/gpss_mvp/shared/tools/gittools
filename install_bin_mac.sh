#!/bin/bash

currentDir="$(pwd)"
echo ${currentDir}

buildDir=build-bin

if [ -d $buildDir ]; then
    cd $buildDir
    make uninstall
    cd $currentDir
    rm -rf $buildDir
fi

mkdir $buildDir
cd $buildDir
cmake .. 
make install
cd $currentDir


