#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se
#Florian Bergner florian.bergner@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
arg1=$1
scriptDir=$(cd $(dirname $sourceDir); pwd)


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    return 1
fi

printDebug()
{
	echo -e "$BLCYAN $@ $NOCOLOR"
}

printGitrPushHelp()
{
	#echo -e "\e[33mHelp -- How to use this alias\e[0m"
	#echo "~\$gitrpush <origin> <main>"

	echo -e "\e[1m NAME\e[0m"
	echo "	gitrpush "
	echo -e "\e[1m SYNOPSIS\e[0m"
	echo "	gitrpush [<current_branch> [remote_branch]]"
	echo -e "\e[1m DESCRIPTION\e[0m"
	echo -e "	The family of functions git(\e[1mr\e[0m)* are standard git commands that are execued recursivelly in all the submodules of the git repo. gitrpush pushes the changes committed in the local branch \e[1mcurrent_branch\e[0m to the remote branch \e[1mremote_branch\e[0m. If \e[1mcurrent_branch\e[0m is not defined, then the standard \"origin\" will be used. In the same way if \e[1mremote_branch\e[0m is not defined, then \"main\" will be used instead. For example \"gitrpush\" will exectue \"git gitrpush origin main, followed by gitdo push origin main\" (see \e[1mgitrdo -h\e[0m)."
	echo -e "\e[1m AUTHORS\e[0m"
	echo -e "	Emmanuel Dean <deane@chalmers.se>"
}


if [ $# -gt 2 ];
then
	echoc BLRED "ERROR: Incorrect number of arguments. See " BWHITE "gitrpush --help"
	printGitrPushHelp
	exit 0
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ];
then
	printGitrPushHelp
	exit 0
fi

echo -e "\e[33mGIT PUSH RECURSIVE in \e[94m${PWD}\e[0m"

or=""
dest=""

if [ $# -eq 0 ]; then
	or="origin"
	dest="main"
	echo -e "\e[32mgitrdo push -u $or $dest\e[0m"		
elif [ $# -eq 1 ]
then
	or="$1"
	dest="main"
	echo -e "\e[32mgitrdo push -u \e[34m$or \e[32m$dest\e[0m"
elif [ $# -eq 2 ]
then
	or="$1"
	dest="$2"	
	echo -e "\e[32mgitrdo push -u \e[34m$or $dest\e[0m"
fi

gitrdo push -u "$or" "$dest"
gitpush "$or" "$dest"
printDebug "New version"

