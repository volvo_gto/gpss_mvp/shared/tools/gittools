#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se
#Florian Bergner florian.bergner@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
arg1=$1
scriptDir=$(cd $(dirname $sourceDir); pwd)


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    return 1
fi


printGitUpHelp()
{
	#echo -e "\e[33mHelp -- How to use this alias\e[0m"
	#echo "~\$gitup <origin> <main>" '"msg"'

	echo -e "\e[1m NAME\e[0m"
	echo "	gitup"
	echo -e "\e[1m SYNOPSIS\e[0m"
	echo "	gitup [<remote> [branch]] <\"message\">"
	echo -e "\e[1m DESCRIPTION\e[0m"
	echo -e "	gitup searches, adds, commits and pushes all the changes made in the local branch \e[1mremote\e[0m to the remote branch \e[1mbranch\e[0m. This is not a recursive function (such as \e[1mgitrup\e[0m), instead this function only updates the changes in current git repo or submodule. In brief this function executes: gitadda, gitco \e[1m\"message\"\e[0m, gitpush \e[1mbranch\e[0m \e[1mremote\e[0m. If \e[1mremote\e[0m is not defined, then the standard \"origin\" will be used. In the same way if \e[1mbranch\e[0m is not defined, then \"main\" will be used instead. This function should be used only with simple git repos. For an iterative method use \e[1mgitrup\e[0m instead."
	echo -e "\e[1m AUTHORS\e[0m"
	echo -e "	Emmanuel Dean <deane@chalmers.se>"
}

if [ $# -eq 0 ]
then
	echoc BRED "ERROR: No arguments supplied. See " BWHITE "gitup --help"
	printGitUpHelp
	exit 1
fi

if [ "$1" == "-h" ] || [ "$1" == "--help" ];
then
	printGitUpHelp
	exit 1
fi

echo -e "\e[33mGIT UP in \e[94m${PWD}\e[0m"

 msg=""
 or=""
 dest=""	

if [ $# -eq 1 ]; then
	or="origin"
	dest="main"
	msg="$1"
	echo -e "\e[32mgitup $or $dest \e[34m\"$msg\"\e[0m"
elif [ $# -eq 2 ]
then
	or="$1"
	dest="main"
   msg="$2"
	echo -e "\e[32mgitup \e[34m$or \e[32m$dest \e[34m\"$msg\"\e[0m"
elif [ $# -eq 3 ]
then
	or="$1"
	dest="$2"
	msg="$3"
	echo -e "\e[32mgitup \e[34m$or $dest \"$msg\"\e[0m"
fi

echo -e "\e[33mGIT ADD ALL \e[33m${PWD}\e[0m"
gitadda
#gitst
echo -e ""


echo -e "\e[33mGIT COMMIT \e[33m${PWD}\e[0m"
gitco "$msg"
echo -e ""

echo -e "\e[33mGIT PUSH \e[33m${PWD}\e[0m"
gitpush "$or" "$dest"
echo -e ""

echo -e "\e[34mGIT STATUS \e[33m${PWD}\e[0m"
gitst	 
