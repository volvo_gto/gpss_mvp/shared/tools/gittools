#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se
#Florian Bergner florian.bergner@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
arg1=$1
scriptDir=$(cd $(dirname $sourceDir); pwd)


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    return 1
fi

printGitRemoteHelp()
{
	echoc BWHITE "NAME"
	echoc NOCOLOR "	gitremote"
	echoc BWHITE "SYNOPSIS"
	echoc NOCOLOR "	gitremote " YELLOW "[remote_name] " LBLUE "<project_url>"
   echoc NOCOLOR "	gitremote " GREEN "[-r] " LBLUE "<project_url_new>"
	echoc NOCOLOR "	gitremote " GREEN "[-r] " YELLOW "<remote_name_old> " LYELLOW "<remote_name_new>"
	echoc NOCOLOR "	gitremote " GREEN "[-r] " YELLOW "<remote_name> " LBLUE "<project_url_new>"
	
	echoc BWHITE " DESCRIPTION"
	echoc NOCOLOR "	gitremote is an alias to " BWHITE "add " NOCOLOR "or " BWHITE "remove " NOCOLOR "remotes to the git project."
	echoc NOCOLOR "	If both " YELLOW "remote_name " NOCOLOR "and " LBLUE "project_url " NOCOLOR "are provided, then " BWHITE "gitremote " NOCOLOR "will add a new remote: " LBLUE "project_url " NOCOLOR "with the give name: " YELLOW "remote_name"  
	echoc NOCOLOR "	If only " LBLUE "project_url " NOCOLOR "is given, then " BWHITE "gitremote " NOCOLOR "will " LGREEN "add " NOCOLOR "the remote with " YELLOW "origin " BWHITE "as the remote_name." 
	echoc NOCOLOR "	If only " YELLOW "remote_name " NOCOLOR "is given, then " BWHITE "gitremote " NOCOLOR "will " LRED "remove " NOCOLOR "that remote. \n"
	echoc NOCOLOR "        The special option " GREEN "-r " NOCOLOR "is used to " BYELLOW "rename " NOCOLOR " a remote, or to " BBLUE "change " NOCOLOR "the remote url, depending on the input arguments. For example: \n"
   echoc BWHITE "        gitremote " GREEN "-r " LBLUE "project_url_new"
	echoc NOCOLOR "        This command will " UWHITE "change" NOCOLOR " the url of the remote " BWHITE "origin "  NOCOLOR "from " BLUE "project_url_old " NOCOLOR "to " LBLUE "project_url_new\n"
	echoc BWHITE "        gitremote " GREEN "-r " YELLOW "remote_name_old " LYELLOW "remote_name_new"
	echoc NOCOLOR "        This command will " UWHITE "rename" YELLOW " remote_name_old " NOCOLOR "with the new remote name " LYELLOW "remote_name_new\n"
	echoc BWHITE "        gitremote " GREEN "-r " YELLOW "remote_name " LBLUE "project_url_new"
	echoc NOCOLOR "        This command will " UWHITE "change" NOCOLOR " the url of the remote " YELLOW "remote_name "  NOCOLOR "from " BLUE "project_url_old " NOCOLOR "to " LBLUE "project_url_new\n"
	echoc BWHITE "AUTHORS"
	echoc NOCOLOR "	Emmanuel Dean <deane@chalmers.se>"
	echoc NOCOLOR "	Florian Bergner <florian.bergner@chalmers.se>"
}


if [ "$1" == "-h" ] || [ "$1" == "--help" ];
then
	printGitRemoteHelp
	exit -1
fi

if [ $# -eq 0 ] || [ $# -gt 4 ]
then
	echoc BLRED "ERROR: Wrong number of arguments, see: " BWHITE "gitremoteadd --help"
	printGitRemoteHelp
	exit -1
fi

#Replace option

if [ "$1" == "-r" ] || [ "$1" == "--rename" ];
then
   if [ $# -lt 2 ] || [ $# -gt 3 ];
   then
      echoc BLRED "ERROR: Wrong number of arguments, see: " BWHITE "gitremoteadd --help"
      printGitRemoteHelp
      exit -1
   fi
   
   if [ $# -eq 2 ];
   then
      if [[ "$2" == *'ssh'* ]] || [[ "$2" == *'sftp'* ]] || [[ "$2" == *'@'* ]];
      then
      
         repo="$2"
         repo_ssh=${repo/"sftp:"/"ssh:"}
      
         echoc YELLOW "Replacing url of " BWHITE "origin " YELLOW "with " BLUE "$repo_ssh"
         git remote set-url "origin" "$repo_ssh"
         giturl
         exit 0
      else
         echoc BRED "ERROR: Wrong use of arguments with " GREEN "-r " BRED "option. You need to provide a url, see: " BWHITE "gitremoteadd --help"
         printGitRemoteHelp
         exit 1
      fi
   fi
   
   
   if [ $# -eq 3 ];
   then
      if [[ "$2" == *'ssh'* ]] || [[ "$2" == *'sftp'* ]] || [[ "$2" == *'@'* ]];
      then
         echoc BRED "ERROR: Wrong use of arguments with " GREEN "-r " BRED "option. You need to provide the remote name first, see: " BWHITE "gitremoteadd --help"
         printGitRemoteHelp
         exit 1
      fi
      
      if [[ "$3" == *'ssh'* ]] || [[ "$3" == *'sftp'* ]] || [[ "$3" == *'@'* ]];
      then
      
         repo="$3"
         repo_ssh=${repo/"sftp:"/"ssh:"}
      
         echoc YELLOW "Replacing url of remote " GREEN "$2 " YELLOW "with " BLUE "$repo_ssh"
         git remote set-url "$2" "$repo_ssh"
         giturl
         exit 0
      else
         echoc YELLOW "Renaming remote " GREEN "$2 " YELLOW "to " BLUE "$3" 
         git remote rename "$2" "$3"
         giturl
         exit 0
      fi
   fi
	exit 0
fi

remote_name=""
repo_ssh=""

#Normal options
# Verify if the arguments are correct

if [ $# -eq 1 ];
then
   if [[ "$1" == *'ssh'* ]] || [[ "$1" == *'sftp'* ]] || [[ "$1" == *'@'* ]];
   then
      remote_name="origin"
      repo="$1"
      repo_ssh=${repo/"sftp:"/"ssh:"}
   else
#       giturl
      
      export remoteList="$(giturl remote)"
      #remoteLists=$(echo $remoteList | tr " " "\n")
      
      while read -r line;
      do
         if [[ "$line" == *"$1"* ]];
         then
            echoc UPURPLE "$line"
         else
            echoc NOCOLOR "$line"
         fi
      done <<< "$remoteList"
      
      echoc BYELLOW "Do you want to remove the remote: " BPURPLE "$1"
      select yn in "Yes" "No"; do
         case $yn in
            Yes ) git remote remove "$1"; giturl; echoc BYELLOW "The remote " BPURPLE "$1 " BYELLOW "was removed!! "; break;;
            No ) giturl; echoc BLRED "The remote " BPURPLE "$1 " BLRED "was not removed!! "; exit;;
         esac
      done
      exit 0
   fi
else
   if [[ "$1" == *'ssh'* ]] || [[ "$1" == *'sftp'* ]] || [[ "$1" == *'@'* ]];
   then
      echoc BRED "Wrong order in the arguments. First you need to define the name of the new remote. See " BWHITE "gitremoteadd --help"
      printGitRemoteHelp
      exit 0
   else
      remote_name="$1"
      repo="$2"
      repo_ssh=${repo/"sftp:"/"ssh:"}
   fi
fi

echoc BYELLOW "Adding new remote: " LGREEN "$repo_ssh " NOCOLOR "with name " BYELLOW "$remote_name "

git remote add "$remote_name" "$repo_ssh"

giturl

