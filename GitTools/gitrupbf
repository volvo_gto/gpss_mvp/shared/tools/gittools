#!/bin/bash
#TUM ICS
#Emmanuel Dean deane@chalmers.se
#Florian Bergner florian.bergner@chalmers.se

# get location of this script
sourceDir=$BASH_SOURCE
runDir=$0
arg1=$1
scriptDir=$(cd $(dirname $sourceDir); pwd)


if [[ $sourceDir != $runDir ]]; then
    echoc BRED "ERROR: You must run this script."
    return 1
fi

printGitrUpdateBFHelp()
{
	#echo -e "\e[33mHelp -- How to use this alias\e[0m"
	#echo "~\$gitrupdatebf <origin> <main> \"msg\""
	echo -e "\e[1m NAME\e[0m"
	echo "	gitrupbf"
	echo -e "\e[1m SYNOPSIS\e[0m"
	echo "	gitrupbf [<current_branch> [remote_branch]] <\"message\">"
	echo -e "\e[1m DESCRIPTION\e[0m"
	echo -e "	The family of functions git(\e[1mr\e[0m)* are standard git commands that are execued recursivelly in all the submodules of the git repo. gitrupbf searches, adds, commits and pushes all the changes made in the local branch \e[1mcurrent_branch\e[0m to the remote branch \e[1mremote_branch\e[0m. This is a brute force function (compared with \e[1mgitrup\e[0m). In brief this function executes: gitradda, gitrcommit \e[1m\"message\"\e[0m, gitrpush \e[1mcurrent_branch\e[0m \e[1mremote_branch\e[0m. If \e[1mcurrent_branch\e[0m is not defined, then the standard \"origin\" will be used. In the same way if \e[1mremote_branch\e[0m is not defined, then \"main\" will be used instead. This function should be used only when a detailed update is required. This function must be exectued as many times as nodes in the git repo. For an iterative method use \e[1mgitrup\e[0m instead."
	echo -e "\e[1m AUTHORS\e[0m"
	echo -e "	Emmanuel Dean <deane@chalmers.se>"
}

#Brute force top-down update. Not suitable for recursive submodule trees
#This alias is for small git projects. Single branch (one .gitmodules list only)
if [ "$1" == "-h" ] || [ "$1" == "--help" ];
then
	printGitrUpdateBFHelp
	exit 1
fi

if [ $# -eq 0 ]
then
	echoc BRED "ERROR: No arguments supplied. See " BWHITE "gitrupbf --help"
	printGitrUpdateBFHelp
	exit 1
fi

echo -e "\e[33mGIT UPDATE RECURSIVE BF in \e[94m${PWD}\e[0m"

 msg=""
 or=""
 dest=""	

if [ $# -eq 1 ]; then
	or="origin"
	dest="main"
	msg="$1"
	echo -e "\e[32mgitrupdatebf $or $dest \e[34m\"$msg\"\e[0m"
elif [ $# -eq 2 ]
then
	or="$1"
	dest="main"
   msg="$2"
	echo -e "\e[32mgitrupdatebf \e[34m$or \e[32m$dest \e[34m\"$msg\"\e[0m"
elif [ $# -eq 3 ]
then
	or="$1"
	dest="$2"
	msg="$3"
	echo -e "\e[32mgitrupdatebf \e[34m$or $dest \"$msg\"\e[0m"
fi

#gitsupdate origin main "msg"
#echo -e "\e[33mGITs STATUS (SUBMODULE)\e[0m"
gitrstbf
echo -e ""

#echo -e "\e[33mGITs ADD ALL (SUBMODULE)\e[0m"
gitradda
echo -e ""

#echo -e "\e[33mGITs COMMIT (SUBMODULE)\e[0m"
gitrco "$msg"
echo -e ""

#echo -e "\e[33mGITs PUSH (SUBMODULE)\e[0m"
gitrpush "$or" "$dest"
echo -e ""

#echo -e "\e[33mGIT ADD ALL (TOP LEVEL)\e[0m"
gitadda
echo -e ""


#echo -e "\e[33mGIT COMMIT (TOP LEVEL)\e[0m"
gitco "$msg"
echo -e ""

#echo -e "\e[33mGIT PUSH (TOP LEVEL)\e[0m"
gitpush "$or" "$dest"
echo -e ""

echo -e "\e[34mGIT FINAL STATUS\e[0m"
gitrstbf 

