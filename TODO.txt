1) gitrup: Avoid multiple commits with the same log message.
2) gitrup: check also for unpushed commits in subgits. Solution: Add git checkout to test if the git repo is dirty and continue from commit and push.
