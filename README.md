# GitTools

This git repo provides a set of extended git tools for handling structured git repos, e.g. git submodules.

## Installation

The following installation instructions have been tested in Ubuntu 20.04.

### Dependencies

These git extensions depend on the **BashTools** package. First you need to download that package and install it. 

Follow the instructions in:

<https://gitlab.com/volvo_gto/gpss_mvp/shared/tools/bashtools>

### Global installation

The GitTools uses CMake to simplify the install and uninstall process. Also for simplicity, we will install these tools globally.

#### 1. Install the GitTools scripts

```bash
git clone git@gitlab.com:volvo_gto/gpss_mvp/shared/tools/gittools.git
cd gittools/
sudo ./install_bin.sh
```

### Getting information

To list all the additional aliases and scripts provided by BashTools you can run in a terminal the following command:

```bash
gitopts
```

You should see something similar to this:

```bash
Git Tools options (gitopt)
Full description for all the git tools listed below can be obtained with the '-h'|'--help' arguments, e.g. gitadd --help

Standard Git                                                                                                                
+ gitadd:                       adds changes to the repo.
+ gitadda:                      adds all changes to the repo.
+ gitclone <url> [folder_name]: clones the git repo url into the dir folder_name.
+ gitco <msg>:                  commits the changes in the git repo, adding the message msg.
+ gitdiff [<branch>][file]:     shows the differences of file file with branch branch.
+ gitfilediff [file]:           shows the differences of file file with the last branch.
+ gitfind:                      searches and lists all dirs named .git recursively.
+ gitrmd:                       searches and removes all dirs named .git recursively.
+ gitfindf:                     searches and lists all files named .git recursively.
+ gitrmf:                       searches and removes all files named .git recursively.
+ gitinirepo <url><path>:       initializes the git repo url into the dir path.
+ gitiniaddrepo <url><path>:    initializes and adds the git repo url as a submodule into the dir path.
+ gitlog:                       shows the log files of the current git repo.
+ gitgreplog <match_pattern>: 
                                searches for match_pattern in the log.
+ gitpull [opts]<current_branch><remote_branch>: 
                                pulls the remote_branch into the current_branch.
+ gitpush [opts]<current_branch><remote_branch>: 
                                pushes the current_branch into the remote_branch.
+ gitst:                        lists all the changes made in the current git repo.
+ gitup [<current_branch>[remote_branch]]: 
                                searches, adds, commits and pushes all the changes made in the current_branch
                                to the remote_branch.
+ giturl:                       shows the url of the current git repo.
+ gitremote:                    adds or remove remotes to the current git repo.
+ gitconflict:                  shows unmerged files found in the current git repo.
+ gitreset [options <message>][commitSHA]: 
                                resets the git repo to the initial/commitSHA using message as commit message.

Recursive Git (submodule foreach)                                                                                            
+ gitrdo <command>[args]:     executes the command with the arguments args in each submodule of the git repo recursivelly.
+ gitradd:                    adds changes in each submodules of the git repo recursivelly.
+ gitradda:                   adds all changes in each submodules of the git repo recursivelly.
+ gitrco <msg>:               commits all the changes with the message msg in each submodule of the git repo recursivelly.
+ gitrdiff <target><current>: shows the differences between the current branch and the targetbranch, 
                              in each submodule of the git repo recursivelly.
+ gitrinup:                   initializes and updates each submodules of the git repo recursivelly.
+ gitrpull [<current>[remote]]:pulls the remote branch into the current branch, 
                              in each submodule of the git repo recursivelly.
+ gitrpush [<current>[remote]]:pushes the commited changes in the current branch into the remote branch, 
                              in each submodule of the git repo recursivelly.
+ gitrst:                     searches and lists all the changes made in the local branch.
+ gitrup [<current>[remote]]<msg>:
                              searches, adds, commits, and pushes all the changes made in the current branch 
                              into the remote branch, in each submodule of the git repo recursivelly.
+ gitrconflict:               shows the unmerged files found in each submodules of the git repo recursivelly.

Submodules Git (management)                                                                                                  
+ gitsadd [opts] <url> <dest_path>: adds the git repo in url as submodule in the path dest_path.
+ gitsinit:                         intializes a submodule.
+ gitsup:                           updates a submodule.
+ gitsiniupcom:                     intializes, updates, and checks a submodule with the main branch.
```

Additionally, each script provides detailed information using the arguments **-h** or **--help**, e.g. 

The following command:

```bash
gitrup -h
```

Will show the detailed information of the script:

```bash
NAME
	gitrup
 SYNOPSIS
	gitrup [<current_branch> [remote_branch]] <"message">
 DESCRIPTION
	The family of functions git(r)* are standard git commands that are execued recursivelly in all the submodules of the git repo. gitrup searches, adds, commits and pushes all the changes made in the local branch current_branch to the remote branch remote_branch. This is not a brute force function (such as gitrupdatebf), instead this function only updates the changes in the submodules that report changes (using gitst). In brief this function executes: gitup current_branch remote_branch "message". If current_branch is not defined, then the standard "origin" will be used. In the same way if remote_branch is not defined, then "main" will be used instead.
 AUTHORS
	Emmanuel Dean <deane@chalmers.de>
```

### Uninstall

To uninstall the scripts just run in the BashTools path

```bash
cd /path/to/GitTools
./uninstall.sh
```

### Examples

A short tutorial on how to use the GitTools with git-submodules can be found here:

https://gitlab.com/volvo_gto/gpss_mvp/shared/tools/gittools/-/wikis/Git-sub-modules




