#!/bin/bash

currentDir="$(pwd)"
echo ${currentDir}

if [ ! -d build-bin ]; then
	echo "ERROR: no build-bin dir"	
	exit 1
fi

cd build-bin
sudo make uninstall
cd $currentDir
rm -rf build-bin



